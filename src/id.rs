/// [Id] type used for uniquely identifying trace points.
pub struct Id {
    id: [u8; 3],
}

impl Id {
    /// Creates an [Id] from an [u32] value.
    ///
    /// Only uses the least significant 20 bits of the value.
    pub const fn from_value(id_value: u32) -> Self {
        let mut temp_id = id_value & 0x000F_FFFF;
        let mut id_array: [u8; 3] = [0; 3];
        id_array[2] = (temp_id & 0xFF) as u8;
        temp_id >>= 8;
        id_array[1] = (temp_id & 0xFF) as u8;
        temp_id >>= 8;
        id_array[0] = (temp_id & 0xFF) as u8;
        Id { id: id_array }
    }
    /// Converts the [Id] into an [u8] array.
    pub const fn into(self) -> [u8; 3] {
        self.id
    }
}

#[cfg(test)]
mod tests {
    use super::Id;

    #[test]
    fn test_from_value() {
        let id: Id = Id::from_value(0xA12345);
        let id_array: [u8; 3] = id.into();
        assert_eq!([0x01, 0x23, 0x45], id_array);
    }
}
