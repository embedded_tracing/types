/// [Timestamp] which is required for trace points.
pub struct Timestamp {
    timestamp: [u8; 5],
}

impl Timestamp {
    /// Creates a [Timestamp] from an [u64] value.
    ///
    /// Only uses the least significant 40 bits of the value.
    pub fn from_value(timestamp_value: u64) -> Self {
        let mut timestamp: [u8; 5] = [0; 5];

        let timestamp_array = timestamp_value.to_be_bytes();

        timestamp[..].copy_from_slice(&timestamp_array[3..]);

        Timestamp { timestamp }
    }
    /// Creates an [Timestamp] from a two [u32] values.
    ///
    /// Some hardware represents 64 bit system tick values with two [u32] integers. This API can be used in such cases.
    ///
    /// The `high` parameter represents the most significant bits of the system tick value. The `low` value the least significant bits.
    ///
    /// Only uses the least significant 8 bits of the `high` value but all bits of the `low` value
    pub fn from_high_low(high: u32, low: u32) -> Self {
        let mut timestamp: [u8; 5] = [0; 5];
        timestamp[0] = (high & 0xFF) as u8;
        let timestamp_array = low.to_be_bytes();

        timestamp[1..].copy_from_slice(&timestamp_array[..]);

        Timestamp { timestamp }
    }
    /// Converts the [Timestamp] into an [u8] array.
    pub fn into(self) -> [u8; 5] {
        self.timestamp
    }
}

#[cfg(test)]
mod tests {
    use super::Timestamp;

    #[test]
    fn test_from_value() {
        let id: Timestamp = Timestamp::from_value(0x12_34_56_78_9A_BC_DE_F0);
        let id_array: [u8; 5] = id.into();
        assert_eq!([0x78, 0x9A, 0xBC, 0xDE, 0xF0], id_array);
    }

    #[test]
    fn test_from_high_low() {
        let id: Timestamp = Timestamp::from_high_low(0xCA_FE_D5_12, 0x34_56_78_9A);
        let id_array: [u8; 5] = id.into();
        assert_eq!([0x12, 0x34, 0x56, 0x78, 0x9A], id_array);
    }
}
