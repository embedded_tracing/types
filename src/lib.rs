//! crate that contains types used in the embedded_tracing ecosystem.
//! Especially for the interfaces between `embedded_tracing` (client library), `embedded_tracing_logger` and `embedded_tracing_time`.

#![no_std]
#![deny(unsafe_code)]
#![warn(missing_docs)]

mod id;
mod timestamp;
mod tracepoint;

pub use id::Id;
pub use timestamp::Timestamp;
pub use tracepoint::{TraceLevel, TracePoint, TraceType};
