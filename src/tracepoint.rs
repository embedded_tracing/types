use crate::id::Id;
use crate::timestamp::Timestamp;

/// The [TraceType] defines the type of the trace point.
///
/// Types enable to identify wether a [TracePoint] is an individual event or comes in a pair of opening and closing span trace points.
#[repr(u8)]
pub enum TraceType {
    /// The trace points describes an opening span.
    SpanOpen = 0b0000_0000,
    /// The trace points describes a closing span.
    SpanClose = 0b0100_0000,
    /// The trace point describes an event.
    Event = 0b1000_0000,
    /// Reserved variant.
    Reserved = 0b1100_0000,
}

/// The [TraceLevel] of the trace point.
///
/// This is usually makes more sense in the context of events and less sense in the context of spans.
///
/// Levels enable to filter out trace points during runtime.
#[repr(u8)]
pub enum TraceLevel {
    /// The trace point has the lowest level for the most verbose messages and spans.
    Trace = 0b0000_0000,
    /// The trace point describes an event that is relevant during debugging.
    Debug = 0b0001_0000,
    /// The trace point describes an event that issues a warning.
    Warn = 0b0010_0000,
    /// The trace point describes an error event.
    Error = 0b0011_0000,
}

/// A [TracePoint], this means a point in time ([Timestamp]) with a type ([TraceType]) a level ([TraceLevel]) and a unique idea.
///
/// It either describes a singular event or it comes in pairs of an opening and closing span trace point.
pub struct TracePoint {
    trace_point: [u8; 8],
}

impl TracePoint {
    /// Create a [TracePoint] from a [Timestamp], a [TraceType], a [TraceLevel] and an [Id].
    pub fn from(
        timestamp: Timestamp,
        trace_type: TraceType,
        trace_level: TraceLevel,
        id: Id,
    ) -> Self {
        let mut trace_point = [0; 8];

        trace_point[0..5].copy_from_slice(&timestamp.into()[..]);

        // follow with trace type and level in the most significant bits
        // of the next byte
        let trace_type_level = trace_type as u8 | trace_level as u8;
        trace_point[5] = trace_type_level;

        // add the id in the last bytes of trace_point
        // the most significant bits are copied into
        // the least significant bits of the byte
        // that already contain the trace type and trace level

        let id_array = id.into();

        trace_point[5] |= id_array[0];

        trace_point[6..].copy_from_slice(&id_array[1..]);

        TracePoint { trace_point }
    }
    /// Converts the [TracePoint] into an [u8] array.
    pub fn into(self) -> [u8; 8] {
        self.trace_point
    }
}

#[cfg(test)]
mod tests {

    use super::{Id, Timestamp, TraceLevel, TracePoint, TraceType};

    #[test]
    fn test_from() {
        let timestamp = Timestamp::from_value(0x11_22_33_44_55);
        let trace_type = TraceType::SpanClose;
        let trace_level = TraceLevel::Warn;
        let id = Id::from_value(0xF6_77_88);
        let trace_point = TracePoint::from(timestamp, trace_type, trace_level, id);
        let data = trace_point.into();
        assert_eq!([0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88], data);
    }
}
