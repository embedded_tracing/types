# embedded_tracing_types - Readme

crate that contains types used in the embedded_tracing ecosystem. Especially for the interfaces between `embedded_tracing` (client library), `embedded_tracing_logger` and `embedded_tracing_time`.

## Usage

Usage of these types is explained in the aforementioned crates.

## License

MIT, see [LICENSE](./LICENSE) file.
